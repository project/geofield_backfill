# Geofield backfill

## Overview
Although this module sounds like something Greenpeace might take issue with, it is actually an environmentally friendly companion module to Geofield (a dependency) that can be used to find any entities with empty Geofield fields, and re-save them to trigger the lat/long lookup. This was created to retroactively do lookups for cases where the original lookup failed due to daily API limits.

For example: You used Migrate or Feeds to import 5000 entities at once, but the daily API limit is 1000, so now you've got 4000 entities with no latitude/longitude.

## Installation
Install and enable the module is normal. For example:

```
drush dl geofield_backfill
drush en geofield_backfill
```

Then configure the module at /admin/config/content/geofield-backfill by specifying which entity types and bundles you would like to "backfill". You can also indicate the number of items you would like to fix per cron run.

Thereafter, on each cron run, the indicated number of items will be backfilled. The results will show up in your Drupal log. If, after attempting the backfill, the item still isn't able to get geocoded, it will be tracked as a "failure" and will be skipped during future cron runs.
