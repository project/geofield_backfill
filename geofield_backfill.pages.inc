<?php

/**
 * Form callback for Geofield Backfill configuration page.
 */
function geofield_backfill_configuration($form, &$form_state) {

  $geofields = _geofield_backfill_get_fields();
  $checkboxes = array();
  foreach ($geofields as $field_name => $entity_types) {
    foreach ($entity_types as $entity_type => $bundles) {
      foreach ($bundles as $bundle) {
        $info = field_info_instance($entity_type, $field_name, $bundle);
        $values = array($entity_type, $bundle, $field_name);
        $key = implode('|', $values);
        $label = $info['label'] . ' (' . implode(' -> ', $values) . ')';
        $checkboxes[$key] = $label;
      }
    }
  }

  $form['geofield_backfill_fields'] = array(
    '#type' => 'checkboxes',
    '#options' => $checkboxes,
    '#default_value' => variable_get('geofield_backfill_fields', array()),
    '#title' => t('Select the Geofield fields you would like to "backfill".'),
    '#description' => t('This module will attempt to populate missing data in these fields on each cron run.'),
  );

  $form['geofield_backfill_limit'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('geofield_backfill_limit', 5),
    '#title' => t('Number of items to backfill on each cron run'),
    '#description' => t('Try to keep in mind any daily API limits when choosing this number.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  return system_settings_form($form);
}

/**
 * Form callback for Geofield Backfill failures page.
 */
function geofield_backfill_failures($form, &$form_state) {

  $failures = db_select('geofield_backfill_fail', 'g')
    ->fields('g')
    ->execute()
    ->fetchAll();
  if (!empty($failures)) {

    $entity_types = array();
    foreach ($failures as $failure) {
      $entity_types[$failure->entity_type][] = $failure->entity_id;
    }
    $links = array();
    foreach ($entity_types as $entity_type => $entity_ids) {
      $entities = entity_load($entity_type, $entity_ids);
      foreach ($entities as $entity) {
        $label = entity_label($entity_type, $entity);
        $uri = entity_uri($entity_type, $entity);
        $links[] = l($label, $uri['path']);
      }
    }
    $form['failures'] = array(
      '#type' => 'fieldset',
      '#title' => t('Geofield Backill Failures'),
      '#description' => t('These items did not successfully get geocoded, and are currently being skipped during Geofield Backfill cron runs.'),
    );

    $form['failures']['list'] = array(
      '#theme' => 'item_list',
      '#items' => $links,
    );

    $form['clear_failures'] = array(
      '#type' => 'submit',
      '#value' => t('Clear Failures'),
      '#description' => t('The failed entities will no longer be skipped during Geofield Backfill cron runs.'),
    );
  }
  else {
    $form['message'] = array(
      '#markup' => t('There are no Geofield Backfill failures currently being tracked.'),
    );
  }

  return $form;
}

/**
 * Submit callback for Geofield Backfill failures page.
 */
function geofield_backfill_failures_submit($form, &$form_state) {

  if ($form_state['submitted']) {
    _geofield_backfill_clear_failures();
  }

  $form_state['rebuild'] = TRUE;
}
